﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bigo
{
    class TreeNode
    {
        public TreeNode LeftNode { get; set; }
        public TreeNode RightNode { get; set; }
        public int value { get; set; }



        public TreeNode(int value)
        {
            this.value = value;
        }


        public bool add(TreeNode node)
        {
            
            if (node.value > value && RightNode == null)
            {
                RightNode = node;
            }
            else if (node.value > value)
            {
                RightNode.add(node);
            }
            else if (node.value < value && LeftNode == null)
            {
                LeftNode = node;
            }
            else if (node.value < value)
            {
                LeftNode.add(node);
            }
            else
            {
                Console.WriteLine("it is equal to a node, so not adding it");
            }
            return true;
        }


        public void traverse()
        {
            if (LeftNode != null)
            {
                LeftNode.traverse();
            }

            Console.WriteLine(value + "," ); 

            if (RightNode != null)
            {
                RightNode.traverse();
            }
        }


    }
}
