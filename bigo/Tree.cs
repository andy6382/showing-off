﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bigo
{
    class Tree
    {
        public TreeNode Root { get; set; }

        public Tree(TreeNode root)
        {
            Root = root;
        }

        public void add (TreeNode newnode)
        {
            Root.add(newnode);
        }


        public void traverse()
        {
            Root.traverse();
        }
    }
}
